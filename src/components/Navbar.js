import React, { useState } from 'react';
import {Link} from 'react-router-dom';

const Navbar=()=>{
    
    return(
        <div>
            <div className="header">
                <nav className="navbar shadow fixed-top navbar-expand-sm navbar-dark bg-success nav-header">
                    <div className="container text-uppercase p-2">
                        <a className="navbar-brand font-weight-bold" href="#">Explore Nepal</a>
                        <ul  className="navbar-nav ml-auto">
                            <li  className="nav-item">
                                <Link  className="nav-link"  to="/">HOME</Link>
                            </li>
                            <li  className="nav-item ">
                                <Link  className="nav-link" to="/about">ABOUT</Link>
                            </li>
                             <li  className="nav-item ">
                                <Link  className="nav-link" to="/contact">CONTACT</Link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>
     <div className="row">
         <div className="col-lg-3 col-md-3 col-3">
            <div className="sidebar-container">
        </div>
    </div>
</div>
 
</div>
    )};
export default Navbar;