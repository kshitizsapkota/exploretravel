import React,{Component} from 'react';
import Modal from 'react-awesome-modal';

export default class Footer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            visible : false
        }
    }

    openModal() {
        this.setState({
            visible : true
        });
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }
    render() {
      return (
       
           <div className="app">
           <div onClick={() => this.openModal()} className="footer-div">
               <div className="mt-5 text-center">
                <p>Copyright @ ExploreNepal 2020. All rights reserved | This website teplate is made with love.</p>
                </div>
            </div>
            <Modal 
                    visible={this.state.visible}
                    width="800"
                    height="120"
                    effect="fadeInLeft"
                    onClickAway={() => this.closeModal()}
                >
                    <div  className="mt-5 text-center" >
                    <p>Copyright @ ExploreNepal 2020. All rights reserved | This website teplate is made with love.</p>
                        <a href="javascript:void(0);" onClick={() => this.closeModal()}>Close</a>
                    </div>
                </Modal>
        </div>
        
      )
    }
  }
  
       
