import React from 'react';
const Home=()=> {
    return (
        <div className="home-div" id="home">
        <div className="container text-center ">
          <h1 className="text-white mb-5 ml-100"><u className="success">WELCOME TO NEPAL</u></h1> 
          <div className="col-md-10">
              
                     <article><p className="text-justify home-para text-white">
                     Nepal, if not ‘the one’ but surely, she is one of the most beautiful countries in Asia.
                        Nestled in the lap of the gigantic Himalayan Range, tiny land-locked Nepal has eight out of ten highest mountains
                        in the world, including Mt. Everest – the tallest in the world. Renowned for its abundance of natural beauty;
                        bio-diversity; ethnic, linguistic, and social diversity; and historical and cultural wealth, the
                        tourist arrivals have grown from just over 6,000 in 1962 to over a million today.  
                         </p></article>
                     <a className="btn btn-success" href="">READ MORE</a> 
                 </div>
        </div>
        
        </div>
    )
}
export default Home;