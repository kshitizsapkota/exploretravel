import React from 'react'
import trekking from '../image/Trekking.jpg';
import rafting from '../image/rafting.jpg';
const About=()=> {
    return (
        <div className="about-div" id="about">
            <section className="aboutus" >
                <div className="container text-center ">
                    <h1 className="text-center font-weight-bold mb-5"><u>ABOUT US</u></h1> 
                         <p className="text-center a-para"><b>Explore Nepal Pvt. Ltd.</b> is travel organization providing best packages for exploring different places of Nepal.
                         We are focused on providing internal and external tourist with the best facilities available in Nepal and provide
                        favourable environment for exploring nature.
                        </p>
                </div>
            <div className="container about">
                <div className="row">
                    <div className="col-lg-6 col-8">
                        <div className="card">
                            <img className="card-img-top" src={trekking} alt="Card image"/>
                         <div className="card-body">
                            <h4 className="card-title">Trekking</h4>
                            <p className="card-text">We are providing a best route of himalayas for trekking. Best season for trekking in Nepal
                            is on October and November.</p>
                            <a href="#" className="btn btn-success">Learn More</a>
                        </div>
                     </div>
                </div>

        <div className="col-lg-6 col-8">
            <div className="card">
                <img className="card-img-top" src={rafting} alt="Card image"/>
            <div className="card-body">
                    <h4 className="card-title">Rafting in High Current Water</h4>
                    <p className="card-text">Nepal have a best high flowing rivers so that rafting is a prime adventure in Nepal.</p>
                    <a href="#" className="btn btn-success">Learn More</a>
            </div>
            </div>
        </div>
    </div>
    </div>
</section>
</div>
    )
}
export default About;