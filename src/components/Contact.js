import React from 'react'

const Contact=()=> {
    return (
        <div className="contact-div" id="contact">
          <section className="contactus" >
            <div className="container text-center ">
             <h1 className="text-center font-weight-bold mb-5 "><u>CONTACT US</u></h1> 
             <p className="c-para">We are here to help and answer any questions you might have. We look forward to hearing from you😃.</p>
            </div>
        <div className="container">
          <div className="row">
            <div className="col-lg-10 col-md-10 col-10 offset-lg-2 offset-md-2 col-1">
              <form>
                <div className="form-group">
                  <input type="text" className="form-control" id="exampleInputname"  placeholder="Enter your name" required autoComplete="off"/>
                </div>
                <div className="form-group">
                  <input type="email" className="form-control " id="exampleInputname"  placeholder="Enter your email address" required autoComplete="off"/>
                </div>
                <div className="form-group">
                 <input type="text" className="form-control " id="exampleInputname"  placeholder="Enter your phone number" required autoComplete="off"/>
                </div>
                <div className="form-group">
                  <textarea className="form-control" placeholder="Enter your message" rows="3" id="comment" required autoComplete="off"/>
                </div>
                <div className="d-flex justify-content-center">
                  <button type="submit" className="btn btn-primary form-button">Submit</button>
                </div>
             </form>
           </div>
          </div>
         </div>
      </section>
      <div className="container text-center">
          <h3 className="text-center font-weight-bold"><u>OUR ADDRESS</u></h3><br/>
      </div> 
      <div className="container align-items-center justify-content-center">
        <div className="row">
          <div className="col-lg-10 col-md-10 col-10 offset-lg-2 offset-md-2 col-1">
            <div className="map">
              <div className="map-responsive">
                  <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14133.582206313296!2d85.
                  30509023164618!3d27.67416697488893!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb19
                  ccc9454ff9%3A0xf196445c0330168a!2sJawalakhel%2C%20Lalitpur%2044600!5e0!3m2!1sen!2snp!4v1601473
                  121321!5m2!1sen!2snp" width="600" height="450" frameBorder="0" style={{border:0}} 
                  allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>
                 </div>
              </div>
             </div>
            </div>
         </div>
        </div>
    )
}
export default Contact;