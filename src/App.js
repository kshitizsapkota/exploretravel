import React,{Component} from 'react';
import {Route, Switch} from 'react-router-dom';
import { BrowserRouter as Router } from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.css';
import Navbar from './components/Navbar';
import './styles/App.css';
import Home from './components/Home';
import About from './components/About';
import Contact from './components/Contact';
import Footer from './components/Footer';

export default class App extends Component {
  render() {
    return (
      <div>
         <div className="app">
      <Router>
            <div>
              <Navbar/> 
               <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/about" component={About}/>
                <Route exact path="/contact" component={Contact}/>
              </Switch>
              <Footer/>
            </div>
          </Router>
          </div>
      </div>
      
    )
  }
}


     
